<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class SunmiCloudPrinter
 *
 * Before using the API, please fill your app_id and app_key value and remerber to go on your partner's capabilities and add "cloud printed" into your app.
 *
 */
class SunmiCloudPrinter
{
  private $rootUrl = "https://openapi.sunmi.com";
  private $app_id = "YOUR_APP_ID";
  private $app_key = "YOUR_APP_KEY";
  private $nonce = "123456";

  public function __construct()
  {
    $this->time = time();
  }

  /**
   * @param String $sn (not required)
   * @param int $page (not required)
   * @return String
   */
  function getOnlineStatus($sn = null, $page = 1)
  {
    $url = "/v2/printer/open/open/device/onlineStatus";
    if ($sn) {
      $post = json_encode([
        "sn" => $sn,
      ]);
    } else {
      $post = json_encode([
        "page_no" => $page,
        "page_size" => 100000,
      ]);
    }
    $headers = $this->generateHeaders($post);
    return $this->sendRequest($url, $headers, $post);
  }

  /**
   * @param $shop_id (required)
   * @param $sn (required)
   * @return String
   */
  function bindShop($shop_id, $sn)
  {
    $url = "/v2/printer/open/open/device/bindShop";
    $post = json_encode([
      "sn" => $sn,
      "shop_id" => (int)$shop_id
    ]);
    $headers = $this->generateHeaders($post);
    return $this->sendRequest($url, $headers, $post);
  }

  /**
   * @param $shop_id (required)
   * @param $sn (required)
   * @return string
   */
  function unbindShop($shop_id, $sn)
  {
    $url = "/v2/printer/open/open/device/unbindShop";
    $post = json_encode([
      "sn" => $sn,
      "shop_id" => (int)$shop_id
    ]);
    $headers = $this->generateHeaders($post);
    return $this->sendRequest($url, $headers, $post);
  }

  /**
   * @param $sn (required)
   * @return string
   */
  function clearPrintJob($sn)
  {
    $url = "/v2/printer/open/open/device/clearPrintJob";
    $post = json_encode([
      "sn" => $sn
    ]);
    $headers = $this->generateHeaders($post);
    return $this->sendRequest($url, $headers, $post);
  }

  /**
   * @param $trade_no (required)
   * @return string
   */
  function printStatus($trade_no)
  {
    $url = "/v2/printer/open/open/ticket/printStatus";
    $post = json_encode([
      "trade_no" => $trade_no
    ]);
    $headers = $this->generateHeaders($post);
    return $this->sendRequest($url, $headers, $post);
  }

  /**
   * @param $id_order (depends on your use case to print ticket, you'll have to edit this function)
   * @param $sn (required)
   * @return string
   */
  function pushContent($id_order, $sn)
  {
    $url = "/v2/printer/open/open/device/pushContent";
    $post = json_encode([
      "trade_no" => (string)rand(1, 10000000),
      "sn" => $sn,
      "order_type" => 1,
      "content" => $this->generateTicket(),
      "count" => 1
    ]);
    $headers = $this->generateHeaders($post);
    return $this->sendRequest($url, $headers, $post);
  }

  // Helpers methods

  /**
   * @param $jsonBody (required)
   * @return string[]
   *
   * This function generate headers to auth with Sunmi API and sign the request as required in the doc
   */
  private function generateHeaders($jsonBody)
  {
    return $headers = [
      "Content-Type:application/json",
      "Sunmi-Appid:$this->app_id",
      "Sunmi-Nonce:$this->nonce",
      "Sunmi-Timestamp:$this->time",
      "Sunmi-Sign:" . hash_hmac("sha256", $jsonBody . "$this->app_id$this->time$this->nonce", $this->app_key),
      "Source:openapi"
    ];
  }

  /**
   * @param $str (required)
   * @return string
   *
   * This function encodes the string in UTF-8 as required by the doc
   */
  private function strToUtf8($str)
  {
    $encode = mb_detect_encoding($str, array("ASCII", 'UTF-8', "GB2312", "GBK", 'BIG5'));
    if ($encode == 'UTF-8') {
      return $str;
    } else {
      return mb_convert_encoding($str, 'UTF-8', $encode);
    }
  }

  /**
   * @return string (hex)
   *
   * This function generate a false ticket. Refers to the docs to know which char you want to print
   */
  private function generateTicket()
  {
    // chr(27).chr(33).chr(48) are font effect (double height and width) in ESC/POS command;
    // chr(27).chr(33).chr(0) are the standard font size in ESC/POS command;
    // 0A is carriage return

    $str = chr(27) . chr(33) . chr(48) . "Supermarket" . chr(27) . chr(33) . chr(0) . chr(0x0A);
    $str = $str . "Table:01       Employee ID:01" . chr(0x0A);
    $str = $str . "Time:12:45:28" . chr(0x0A);
    $str = $str . "Order No:00554789713585" . chr(0x0A);
    $str = $str . "Item     Price*Num       Amount" . chr(0x0A);
    $str = $str . "--------------------------------" . chr(0x0A);
    $str = $str . "Item1     7.00*1      7.00" . chr(0x0A);
    $str = $str . "Item2     8.00*1      8.00" . chr(0x0A);
    $str = $str . "Item3     2.50*1      2.50" . chr(0x0A);
    $str = $str . "Item4     10.00*1     10.00" . chr(0x0A);
    $str = $str . "Item5     10.00*1     10.00" . chr(0x0A);
    $str = $str . "--------------------------------" . chr(0x0A);
    $str = $str . "Total items:5  Total RMB: 27.00" . chr(0x0A);
    $str = $str . chr(0x0A);
    $str = $str . chr(27) . chr(33) . chr(16) . "      Wish you a good day" . chr(27) . chr(33) . chr(0) . chr(0x0A);//The font shown is in double height and width; 0A is carriage return

    echo $str;

    $orderData = bin2hex($this->strToUtf8($str));

    return $orderData;
  }

  /**
   * @param $url (required)
   * @param $headers (required)
   * @param $post (required)
   * @return string
   *
   * Send the curl request with the goods arguments
   */
  private function sendRequest($url, $headers, $post)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "{$this->rootUrl}$url");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    return curl_exec($ch);
  }
}

