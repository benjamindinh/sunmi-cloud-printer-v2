# Sunmi cloud printer V2 in PHP

A small class in order to send Sunmi request using v2

### A realy easy class (because the docs is only available in chineses for the moment) ready to use

# How to use
Just instance the class.

# Functions

#### getOnlineStatus($sn = null, $page = 1) - Check if a device is logged or not. 
Both fields not required
#### bindShop($shop_id, $sn) - Link a device to a shop
Both fields required
#### unbindShop($shop_id, $sn) - Unlink a device to a shop
Both fields required
#### clearPrintJob($sn) - Clear the print list from a device
Field required
#### printStatus($trade_no) - Check a print's status 
Field required
#### pushContent($id_order, $sn) - Send a ticket to print - Method todo to adapt your use case
Both fields required

